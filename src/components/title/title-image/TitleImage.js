import { Component } from "react";

import backgroundImg from "../../../assets/images/background.jpg";

class TitleImage extends Component {
    render() {
        return (
            <div className="row mt-3">
                <img src={backgroundImg} alt="background" width={500} />
            </div>
        )
    }
}

export default TitleImage;