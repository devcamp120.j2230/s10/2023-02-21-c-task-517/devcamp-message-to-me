import React, { Component } from "react";
import InputMessage from "./input-message/InputMessage";
import LikeImage from "./like-image/LikeImage";

class ContentComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            inputMessage: "",
            outputMessage: [],
            likeDisplay: false
        }
    }

    inputChangeHandler = (message) => {
        this.setState({
            inputMessage: message
        })
    }

    clickHandler = () => {
        if(this.state.inputMessage) {
            this.setState({
                outputMessage: [...this.state.outputMessage, this.state.inputMessage],
                likeDisplay: true
            })
        }
    }

    render() {
        return (
            <React.Fragment>
                <InputMessage 
                    inputMessageProp={this.state.inputMessage}
                    inputChangeHandlerProp={this.inputChangeHandler} 
                    clickHandlerProp={this.clickHandler}
                />
                <LikeImage 
                    outputMessageProp={this.state.outputMessage} 
                    likeDisplayProp={this.state.likeDisplay} 
                />
            </React.Fragment>
        )
    }
}

export default ContentComponent;