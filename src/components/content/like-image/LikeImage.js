import { Component } from "react";

import likeImg from "../../../assets/images/like.png";

class LikeImage extends Component {
    render() {
        const {outputMessageProp, likeDisplayProp} = this.props;

        return (
            <div className="row mt-3">
                <div>{outputMessageProp.map((element, index) => {
                    return <p key={index}>{element}</p>
                })}</div>
                { likeDisplayProp ? <img src={likeImg} alt="like" style={{width: "100px", margin: "0 auto"}}/> : null } 
            </div>
        )
    }
}

export default LikeImage;